interface ICardProductDetails {
    productPage: string;            // URL to the product (relative to MKM's base URL)
    image: string;              // Path to the product's image
    expIcon: number;            // Index of the expansion icon
    trendingPrice: number;      // Price guide entity '''(ATTN: not returned for expansion requests)'''
}