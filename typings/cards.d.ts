interface ICardSet {
    [name: string]: ICard;
}

interface ILegality {
    format: string;
    legality: string;
}

interface IRuling {
    date: string;
    text: string;
}

interface ICard {
    id?: string;
    name: string;
    type?: string;
    types?: string[];
    subtypes?: string[];
    text?: string;
    cmc?: number;
    rarity?: string;
    colors?: string[];
    manaCost?: string;
    rulings?: IRuling[];
    legalities?: ILegality[];
}