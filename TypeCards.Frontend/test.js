/// <reference path="../Typings/es6-promise.d.ts" />
/// <reference path="../Typings/angular.d.ts" />
/// <reference path="../Typings/angular-route.d.ts" />
/// <reference path="../Typings/underscore.d.ts" />
/// <reference path="../../Typings/cards.d.ts" />
/// <reference path="../../Typings/cardproduct.d.ts" />
/// <chutzpah_reference path="//Scripts/angular.js" />
/// <chutzpah_reference path="//Scripts/angular-route.js" />
/// <chutzpah_reference path="//Scripts/angular-mocks.js" />
var AngularMobileGatherer;
(function (AngularMobileGatherer) {
    var mobileGatherer = angular.module("MobileGatherer", ["ngRoute", "angular-loading-bar", "focusOn", "ui.bootstrap"])
        .config([
        "$routeProvider",
        function ($routeProvider) {
            var mainPage = {
                templateUrl: "main.html",
                controller: "MainPageController",
                reloadOnSearch: false
            };
            $routeProvider.
                when("/", mainPage).
                when("/#/", mainPage).
                otherwise({
                redirectTo: "/"
            });
        }
    ]);
    mobileGatherer.controller("HeaderController", [
        "$scope", "$location",
        function ($scope, $location) {
            $scope.isActive = function (viewLocation) { return (viewLocation === $location.path()); };
        }
    ]);
    mobileGatherer.controller("MainPageController", [
        "$scope", "focus", "$http", "quicksearch", "productsearch",
        function ($scope, focus, $http, quicksearch, productSearch) {
            focus("typeaheadFocus");
            $scope.getCards = function (query) { return quicksearch.search(query); };
            $scope.onSelect = function (card) {
                $scope.details = card;
                return productSearch.search(card.name).then(function (products) {
                    // If this card is still the one we're interested in when we get the callback.
                    if ($scope.details === card) {
                        $scope.details.products = products;
                        // Grab the image from products too, since we don't have images for all cards.
                        if (products.length > 0) {
                            $scope.details.cardUrl = products[0].image;
                        }
                    }
                }, function () {
                    $scope.details.error = "Failed to get product details";
                });
            };
        }
    ]);
    mobileGatherer.directive("typeaheadsearch", [
        "$location", "focus", function ($location, focus) {
            var directive = {
                restrict: "E",
                link: function (scope) {
                    var oldOnSelect = scope.onSelect;
                    scope.onSelect = function ($item, $model, $label) {
                        setTimeout(function () {
                            document.activeElement.blur();
                        }, 10);
                        oldOnSelect($item, $model, $label);
                    };
                    scope.focusTypeahead = function ($event) {
                        $event.preventDefault();
                        scope.lookupQuery = "";
                        focus("typeaheadFocus");
                    };
                },
                templateUrl: "partials/typeahead_search.html"
            };
            return directive;
        }]);
    var QuickSearch = (function () {
        function QuickSearch($http) {
            this.$http = $http;
        }
        QuickSearch.prototype.search = function (query) {
            return this.$http.get('/quicksearch', {
                params: { q: query }
            }).then(function (response) {
                return response.data;
            });
        };
        return QuickSearch;
    }());
    mobileGatherer.factory("quicksearch", ["$http", function ($http) {
            return new QuickSearch($http);
        }]);
    var ProductSearch = (function () {
        function ProductSearch($http) {
            this.$http = $http;
        }
        ProductSearch.prototype.search = function (cardName) {
            return this.$http.get('/productsearch', {
                params: { q: cardName }
            }).then(function (response) {
                var products = response.data;
                if (products.length) {
                    for (var i = 0; i < products.length; i++) {
                        products[i].iconX = 21 * ((products[i].expIcon % 10) | 0);
                        products[i].iconY = 21 * ((products[i].expIcon / 10) | 0);
                    }
                }
                return products;
            });
        };
        return ProductSearch;
    }());
    mobileGatherer.factory("productsearch", ["$http", function ($http) {
            return new ProductSearch($http);
        }]);
})(AngularMobileGatherer || (AngularMobileGatherer = {}));
/// <reference path="../Typings/angular.d.ts" />
/// <reference path="../Typings/angular-mocks.d.ts" />
/// <reference path="../Typings/angular-route.d.ts" />
/// <reference path="../typings/jasmine.d.ts" />
describe("MobileGatherer module", function () {
    beforeEach(module("MobileGatherer"));
    var $controller;
    beforeEach(inject(function (_$controller_) {
        $controller = _$controller_;
    }));
    describe("Main Page Controller route (/)", function () {
        it("should use the correct controller", function () {
            inject(function ($route) {
                expect($route.routes["/"].controller).toBe("MainPageController");
            });
        });
        it("should load the correct template URL", function () {
            inject(function ($route) {
                expect($route.routes["/"].templateUrl).toEqual("main.html");
            });
        });
    });
    describe('MainPageController', function () {
        var $httpBackend;
        beforeEach(inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
        }));
        afterEach(function () {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });
        it('should focus the typeaheadFocus field', function () {
            var $scope = {};
            var focusItem;
            var controller = $controller('MainPageController', { $scope: $scope, focus: function (str) { focusItem = str; } });
            expect(focusItem).toBe("typeaheadFocus");
        });
        describe("when quicksearching", function () {
            it("should request /quicksearch with parameter", function () {
                var myObject = {};
                $httpBackend.expectGET("/quicksearch?q=Air+Elemental").respond(200, myObject);
                var $scope = {};
                var controller = $controller('MainPageController', { $scope: $scope, focus: function (str) { } });
                $scope.getCards("Air Elemental").then(function (data) {
                    expect(data).toEqual(myObject);
                });
                $httpBackend.flush();
            });
        });
        describe("when selecting a card", function () {
            it("should set scope details", function () {
                var card = { name: "cool card" };
                $httpBackend.expectGET("/productsearch?q=cool+card").respond(404);
                var $scope = {};
                var controller = $controller('MainPageController', { $scope: $scope, focus: function (str) { } });
                $scope.onSelect(card);
                expect($scope.details).toBe(card);
                $httpBackend.flush();
            });
            it("should get products for card and set image", function () {
                var card = { name: "cool card" };
                var products = [{ expIcon: 15, image: "image test", productPage: "page", trendingPrice: 13.37 }];
                $httpBackend.expectGET("/productsearch?q=cool+card").respond(200, products);
                var $scope = {};
                var controller = $controller('MainPageController', { $scope: $scope, focus: function (str) { } });
                $scope.onSelect(card);
                $httpBackend.flush();
                products[0].iconX = 105;
                products[0].iconY = 21;
                expect($scope.details.products).toEqual(products);
                expect($scope.details.cardUrl).toEqual("image test");
            });
            describe("if card changes while product searching", function () {
                it("should not set card details when product search finishes", function () {
                    var card = { name: "cool card" };
                    var products = [{ expIcon: 15, image: "image test", productPage: "page", trendingPrice: 13.37 }];
                    $httpBackend.expectGET("/productsearch?q=cool+card").respond(200, products);
                    var $scope = {};
                    var controller = $controller('MainPageController', { $scope: $scope, focus: function (str) { } });
                    $scope.onSelect(card);
                    $scope.details = { name: "very cool card" };
                    $httpBackend.flush();
                    expect($scope.details.products).toBeUndefined();
                });
            });
            describe("if product search fails", function () {
                it("should set error", function () {
                    var card = { name: "cool card" };
                    $httpBackend.expectGET("/productsearch?q=cool+card").respond(404);
                    var $scope = {};
                    var controller = $controller('MainPageController', { $scope: $scope, focus: function (str) { } });
                    $scope.onSelect(card);
                    $httpBackend.flush();
                    expect($scope.details.error).toEqual("Failed to get product details");
                });
            });
        });
    });
});
