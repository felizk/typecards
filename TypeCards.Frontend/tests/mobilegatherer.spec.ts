﻿/// <reference path="../Typings/angular.d.ts" />
/// <reference path="../Typings/angular-mocks.d.ts" />
/// <reference path="../Typings/angular-route.d.ts" />
/// <reference path="../typings/jasmine.d.ts" />

describe("MobileGatherer module", () => {
    beforeEach(module("MobileGatherer"));

    var $controller;

    beforeEach(inject(function (_$controller_) {
        $controller = _$controller_;
    }));

    describe("Main Page Controller route (/)", () => {
        it("should use the correct controller", () => {
            inject($route => {
                expect($route.routes["/"].controller).toBe("MainPageController");
            });
        });

        it("should load the correct template URL", () => {
            inject($route => {
                expect($route.routes["/"].templateUrl).toEqual("main.html");
            });
        });
    });

    describe('MainPageController', () => {

        var $httpBackend: ng.IHttpBackendService;
        beforeEach(inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
        }));

        afterEach(function () {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        it('should focus the typeaheadFocus field', () => {

            var $scope: any = {};
            var focusItem;
            var controller = $controller('MainPageController', { $scope: $scope, focus: (str) => { focusItem = str; } });

            expect(focusItem).toBe("typeaheadFocus");
        });

        describe("when quicksearching", () => {
            it("should request /quicksearch with parameter", () => {
                var myObject = {};

                $httpBackend.expectGET("/quicksearch?q=Air+Elemental").respond(200, myObject);

                var $scope: any = {};
                var controller = $controller('MainPageController', { $scope: $scope, focus: (str) => { } });

                $scope.getCards("Air Elemental").then(data => {
                    expect(data).toEqual(myObject);
                });

                $httpBackend.flush();
            });
        });

        describe("when selecting a card", () => {
            it("should set scope details", () => {
                var card: ICard = { name: "cool card" };

                $httpBackend.expectGET("/productsearch?q=cool+card").respond(404);

                var $scope: any = {};
                var controller = $controller('MainPageController', { $scope: $scope, focus: (str) => { } });

                $scope.onSelect(card);

                expect($scope.details).toBe(card);

                $httpBackend.flush();
            });

            it("should get products for card and set image", () => {
                var card: ICard = { name: "cool card" };

                var products: ICardProductDetails[] = [{ expIcon: 15, image: "image test", productPage: "page", trendingPrice: 13.37 }];

                $httpBackend.expectGET("/productsearch?q=cool+card").respond(200, products);

                var $scope: any = {};
                var controller = $controller('MainPageController', { $scope: $scope, focus: (str) => { } });

                $scope.onSelect(card);

                $httpBackend.flush();

                (<any>products[0]).iconX = 105;
                (<any>products[0]).iconY = 21;

                expect($scope.details.products).toEqual(products);
                expect($scope.details.cardUrl).toEqual("image test");
            });

            describe("if card changes while product searching", () => {
                it("should not set card details when product search finishes", () => {
                    var card: ICard = { name: "cool card" };

                    var products: ICardProductDetails[] = [{ expIcon: 15, image: "image test", productPage: "page", trendingPrice: 13.37 }];

                    $httpBackend.expectGET("/productsearch?q=cool+card").respond(200, products);

                    var $scope: any = {};
                    var controller = $controller('MainPageController', { $scope: $scope, focus: (str) => { } });

                    $scope.onSelect(card);
                    $scope.details = { name: "very cool card" };
                    $httpBackend.flush();

                    expect($scope.details.products).toBeUndefined();
                });
            });

            describe("if product search fails", () => {
                it("should set error", () => {
                    var card: ICard = { name: "cool card" };

                    $httpBackend.expectGET("/productsearch?q=cool+card").respond(404);

                    var $scope: any = {};
                    var controller = $controller('MainPageController', { $scope: $scope, focus: (str) => { } });

                    $scope.onSelect(card);
                    $httpBackend.flush();

                    expect($scope.details.error).toEqual("Failed to get product details");
                });
            });
        });
    });

});
