﻿/// <reference path="../Typings/es6-promise.d.ts" />
/// <reference path="../Typings/angular.d.ts" />
/// <reference path="../Typings/angular-route.d.ts" />
/// <reference path="../Typings/underscore.d.ts" />
/// <reference path="../../Typings/cards.d.ts" />
/// <reference path="../../Typings/cardproduct.d.ts" />
/// <chutzpah_reference path="//Scripts/angular.js" />
/// <chutzpah_reference path="//Scripts/angular-route.js" />
/// <chutzpah_reference path="//Scripts/angular-mocks.js" />

module AngularMobileGatherer {
    var mobileGatherer = angular.module("MobileGatherer", ["ngRoute", "angular-loading-bar", "focusOn", "ui.bootstrap"])
        .config([
            "$routeProvider",
            ($routeProvider : ng.route.IRouteProvider) => {

                var mainPage = {
                    templateUrl: "main.html",
                    controller: "MainPageController",
                    reloadOnSearch: false
                };

                $routeProvider.
                    when("/", mainPage).
                    when("/#/", mainPage).
                    otherwise({
                        redirectTo: "/"
                    });
            }
        ]);

    mobileGatherer.controller("HeaderController", [
        "$scope", "$location",
        ($scope, $location: ng.ILocationService) => {
            $scope.isActive = viewLocation => (viewLocation === $location.path());
        }
    ]);

    mobileGatherer.controller("MainPageController", [
        "$scope", "focus", "$http", "quicksearch", "productsearch",
        ($scope, focus, $http: ng.IHttpService, quicksearch: QuickSearch, productSearch: ProductSearch) => {
            focus("typeaheadFocus");

            $scope.getCards = (query) => quicksearch.search(query);

            $scope.onSelect = (card : ICard) => {
                $scope.details = card;

                return productSearch.search(card.name).then(products => {
                    // If this card is still the one we're interested in when we get the callback.
                    if ($scope.details === card) {
                        $scope.details.products = products;

                        // Grab the image from products too, since we don't have images for all cards.
                        if (products.length > 0) {
                            $scope.details.cardUrl = products[0].image;
                        }
                    }
                }, () => {
                    $scope.details.error = "Failed to get product details";
                });
            }
        }
    ]);

    mobileGatherer.directive("typeaheadsearch", [
        "$location", "focus", ($location, focus) => {
            var directive = {
                restrict: "E",
                link(scope) {
                    var oldOnSelect = scope.onSelect;
                    scope.onSelect = ($item, $model, $label) => {
                        setTimeout(() => {
                            (<HTMLElement>document.activeElement).blur();
                        }, 10);
                        oldOnSelect($item, $model, $label);
                    };

                    scope.focusTypeahead = $event => {
                        $event.preventDefault();
                        scope.lookupQuery = "";
                        focus("typeaheadFocus");
                    };
                },
                templateUrl: "partials/typeahead_search.html"
            };
            return directive;
        }]);

    class QuickSearch {
        constructor(private $http: ng.IHttpService) { }

        public search(query: string): Thenable<ICard[]> {
            return this.$http.get('/quicksearch', {
                params: { q: query }
            }).then(response => {
                return response.data;
            });
        }
    }

    mobileGatherer.factory("quicksearch", ["$http", ($http: ng.IHttpService) => {
        return new QuickSearch($http);
    }]);

    class ProductSearch {
        constructor(private $http: ng.IHttpService) { }

        public search(cardName: string): Thenable<ICardProductDetails[]> {
            return this.$http.get('/productsearch', {
                params: { q: cardName }
            }).then(response => {
                var products = <ICardProductDetails[]>response.data;
                if (products.length) {
                    for (var i = 0; i < products.length; i++) {
                        // This is an interesting hack to make use of the expIcon field from our MKM product data.
                        // expIcon is an index into a sprite-sheet of expansion icons all of which are 21x21px
                        // The sprite-sheet has 10 columns.
                        // This hack calculates the proper location of each of the expansion icons based on the index.
                        (<any>products[i]).iconX = 21 * ((products[i].expIcon % 10) | 0);
                        (<any>products[i]).iconY = 21 * ((products[i].expIcon / 10) | 0);
                    }
                }
                return products;
            });
        }
    }

    mobileGatherer.factory("productsearch", ["$http", ($http: ng.IHttpService) => {
        return new ProductSearch($http);
    }]);
}