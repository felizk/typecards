var AngularMobileGatherer;
(function (AngularMobileGatherer) {
    var mobileGatherer = angular.module("MobileGatherer", ["ngRoute", "focusOn"])
        .config([
        "$routeProvider",
        function ($routeProvider) {
            var mainPage = {
                templateUrl: "main.html",
                controller: "MainPageController",
                reloadOnSearch: false
            };
            $routeProvider.
                when("/", mainPage).
                when("/#/", mainPage).
                otherwise({
                redirectTo: "/"
            });
        }
    ]);
    mobileGatherer.controller("HeaderController", [
        "$scope", "$location",
        function ($scope, $location) {
            $scope.isActive = function (viewLocation) { return (viewLocation === $location.path()); };
        }
    ]);
    mobileGatherer.controller("MainPageController", [
        "$scope", "focus",
        function ($scope, focus) {
            focus("typeaheadFocus");
        }
    ]);
    mobileGatherer.directive("typeaheadsearch", [
        "$location", "focus", function ($location, focus) {
            var directive = {
                restrict: "E",
                link: function (scope) {
                    var oldOnSelect = scope.onSelect;
                    scope.onSelect = function ($item, $model, $label) {
                        setTimeout(function () {
                            document.activeElement.blur();
                        }, 10);
                        oldOnSelect($item, $model, $label);
                    };
                    scope.focusTypeahead = function ($event) {
                        $event.preventDefault();
                        scope.lookupQuery = "";
                        focus("typeaheadFocus");
                    };
                },
                templateUrl: "partials/typeahead_search.html"
            };
            return directive;
        }]);
})(AngularMobileGatherer || (AngularMobileGatherer = {}));
