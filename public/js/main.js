/// <reference path="../Typings/es6-promise.d.ts" />
/// <reference path="../Typings/angular.d.ts" />
/// <reference path="../Typings/angular-route.d.ts" />
/// <reference path="../Typings/underscore.d.ts" />
/// <reference path="../../Typings/cards.d.ts" />
/// <reference path="../../Typings/cardproduct.d.ts" />
/// <chutzpah_reference path="//Scripts/angular.js" />
/// <chutzpah_reference path="//Scripts/angular-route.js" />
/// <chutzpah_reference path="//Scripts/angular-mocks.js" />
var AngularMobileGatherer;
(function (AngularMobileGatherer) {
    var mobileGatherer = angular.module("MobileGatherer", ["ngRoute", "angular-loading-bar", "focusOn", "ui.bootstrap"])
        .config([
        "$routeProvider",
        function ($routeProvider) {
            var mainPage = {
                templateUrl: "main.html",
                controller: "MainPageController",
                reloadOnSearch: false
            };
            $routeProvider.
                when("/", mainPage).
                when("/#/", mainPage).
                otherwise({
                redirectTo: "/"
            });
        }
    ]);
    mobileGatherer.controller("HeaderController", [
        "$scope", "$location",
        function ($scope, $location) {
            $scope.isActive = function (viewLocation) { return (viewLocation === $location.path()); };
        }
    ]);
    mobileGatherer.controller("MainPageController", [
        "$scope", "focus", "$http", "quicksearch", "productsearch",
        function ($scope, focus, $http, quicksearch, productSearch) {
            focus("typeaheadFocus");
            $scope.getCards = function (query) { return quicksearch.search(query); };
            $scope.onSelect = function (card) {
                $scope.details = card;
                return productSearch.search(card.name).then(function (products) {
                    // If this card is still the one we're interested in when we get the callback.
                    if ($scope.details === card) {
                        $scope.details.products = products;
                        // Grab the image from products too, since we don't have images for all cards.
                        if (products.length > 0) {
                            $scope.details.cardUrl = products[0].image;
                        }
                    }
                }, function () {
                    $scope.details.error = "Failed to get product details";
                });
            };
        }
    ]);
    mobileGatherer.directive("typeaheadsearch", [
        "$location", "focus", function ($location, focus) {
            var directive = {
                restrict: "E",
                link: function (scope) {
                    var oldOnSelect = scope.onSelect;
                    scope.onSelect = function ($item, $model, $label) {
                        setTimeout(function () {
                            document.activeElement.blur();
                        }, 10);
                        oldOnSelect($item, $model, $label);
                    };
                    scope.focusTypeahead = function ($event) {
                        $event.preventDefault();
                        scope.lookupQuery = "";
                        focus("typeaheadFocus");
                    };
                },
                templateUrl: "partials/typeahead_search.html"
            };
            return directive;
        }]);
    var QuickSearch = (function () {
        function QuickSearch($http) {
            this.$http = $http;
        }
        QuickSearch.prototype.search = function (query) {
            return this.$http.get('/quicksearch', {
                params: { q: query }
            }).then(function (response) {
                return response.data;
            });
        };
        return QuickSearch;
    }());
    mobileGatherer.factory("quicksearch", ["$http", function ($http) {
            return new QuickSearch($http);
        }]);
    var ProductSearch = (function () {
        function ProductSearch($http) {
            this.$http = $http;
        }
        ProductSearch.prototype.search = function (cardName) {
            return this.$http.get('/productsearch', {
                params: { q: cardName }
            }).then(function (response) {
                var products = response.data;
                if (products.length) {
                    for (var i = 0; i < products.length; i++) {
                        // This is an interesting hack to make use of the expIcon field from our MKM product data.
                        // expIcon is an index into a sprite-sheet of expansion icons all of which are 21x21px
                        // The sprite-sheet has 10 columns.
                        // This hack calculates the proper location of each of the expansion icons based on the index.
                        products[i].iconX = 21 * ((products[i].expIcon % 10) | 0);
                        products[i].iconY = 21 * ((products[i].expIcon / 10) | 0);
                    }
                }
                return products;
            });
        };
        return ProductSearch;
    }());
    mobileGatherer.factory("productsearch", ["$http", function ($http) {
            return new ProductSearch($http);
        }]);
})(AngularMobileGatherer || (AngularMobileGatherer = {}));
//# sourceMappingURL=main.js.map