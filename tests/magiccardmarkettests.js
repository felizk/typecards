"use strict";
var mkm = require("../source/magiccardmarket");
var assert = require("assert");
describe("AuthorizationHeaderGenerator", function () {
    // Check that for a specific URL the data is correct
    describe("given specific url, nonce, time, appKey and appSecret when generating header", function () {
        var generator;
        beforeEach(function () {
            generator = new mkm.AuthorizationHeaderGenerator("abcd1234abcd1234", "efgh5678efgh5678", function () { return "nonrandomnonce"; }, function () { return 13371337; });
        });
        it('generates expected signature', function () {
            var header = generator.generateAuthorizationHeader("http://localhost/");
            // This is a pregenerated signature made from a known working state:
            assert(header.indexOf('oauth_signature="/Wfuu07Q6a/KQHfpuhwzP49JxFo="') != -1);
        });
        it('should be in correct format', function () {
            var header = generator.generateAuthorizationHeader("http://localhost/");
            // Pretested regex
            assert(header.match(/OAuth (\w+="[^"]*", )+(\w+="[^"]*")/));
        });
        it('should contain appKey', function () {
            var header = generator.generateAuthorizationHeader("http://localhost/");
            assert(header.indexOf('oauth_consumer_key="abcd1234abcd1234"') != -1);
        });
        it('should contain realm', function () {
            var header = generator.generateAuthorizationHeader("http://localhost/");
            assert(header.indexOf('realm="http://localhost/"') != -1);
        });
        it('should contain nonce', function () {
            var header = generator.generateAuthorizationHeader("nonrandomnonce");
            assert(header.indexOf('oauth_nonce="nonrandomnonce"') != -1);
        });
        it('should contain signature method', function () {
            var header = generator.generateAuthorizationHeader("nonrandomnonce");
            assert(header.indexOf('oauth_signature_method="HMAC-SHA1"') != -1);
        });
        it('should contain timestamp', function () {
            var header = generator.generateAuthorizationHeader("nonrandomnonce");
            assert(header.indexOf('oauth_timestamp="13371337"') != -1);
        });
        it('should contain version', function () {
            var header = generator.generateAuthorizationHeader("nonrandomnonce");
            assert(header.indexOf('oauth_version="1.0"') != -1);
        });
    });
});
var MockCardProductService = (function () {
    function MockCardProductService(cards) {
        this.cards = cards;
    }
    MockCardProductService.prototype.request = function (url) {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.requestedUrl = url;
                resolve({ product: _this.cards });
            }, 0);
        });
    };
    return MockCardProductService;
}());
describe("CardProductProvider", function () {
    it("should map magiccardmarket products to generic card products", function (done) {
        var mkmProducts = [
            {
                website: "/test website",
                image: "test image",
                expIcon: 1337,
                priceGuide: { SELL: 0, LOW: 0, LOWEX: 0, LOWFOIL: 0, AVG: 0, TREND: 42 }
            },
            {
                website: "/test website 2",
                image: "test image 2",
                expIcon: 1338,
                priceGuide: { SELL: 0, LOW: 0, LOWEX: 0, LOWFOIL: 0, AVG: 0, TREND: 43 }
            }];
        var serviceMock = new MockCardProductService(mkmProducts);
        var cardProductProvider = new mkm.CardProductProvider(serviceMock);
        cardProductProvider.requestCardProducts("anything").then(function (cards) {
            assert(cards.length == 2);
            assert.deepEqual(cards[0], {
                productPage: "https://www.magiccardmarket.eu/test website",
                image: "https://www.magiccardmarket.eu/test image",
                expIcon: 1337,
                trendingPrice: 42
            });
            assert.deepEqual(cards[1], {
                productPage: "https://www.magiccardmarket.eu/test website 2",
                image: "https://www.magiccardmarket.eu/test image 2",
                expIcon: 1338,
                trendingPrice: 43
            });
            done();
        });
    });
    it("should access the correct URL with url encoded parameter", function (done) {
        var mkmProducts = [
            {
                website: "/test website",
                image: "test image",
                expIcon: 1337,
                priceGuide: { SELL: 0, LOW: 0, LOWEX: 0, LOWFOIL: 0, AVG: 0, TREND: 42 }
            }];
        var serviceMock = new MockCardProductService(mkmProducts);
        var cardProductProvider = new mkm.CardProductProvider(serviceMock);
        cardProductProvider.requestCardProducts("card with spaces").then(function (cards) {
            assert(serviceMock.requestedUrl == "https://www.mkmapi.eu/ws/v1.1/output.json/products/card%20with%20spaces/1/1/false");
            done();
        });
    });
});
//# sourceMappingURL=magiccardmarkettests.js.map