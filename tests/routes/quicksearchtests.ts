﻿/// <reference path="../../typings/mocha.d.ts" />
/// <reference path="../../typings/cards.d.ts" />
import _ = require("underscore");
import cards = require("../../source/cards");
import quicksearch = require("../../source/routes/quicksearch");
import assert = require("assert");

describe("QuickSeachRoute", () => {

    describe("AutoComplete", () => {

        it("should return an empty array if query length is less than 3", () => {
            var cardList: ICard[] =
                [
                    { name: "carda" },
                    { name: "cardb" },
                ];

            var quickSearchRoute = new quicksearch.QuickSearchRoute(cardList, 10);

            var result = quickSearchRoute.autoComplete("ca");

            assert.equal(result.length, 0);
        });

        it("should return cards with names that contain the search string", () => {
            var cardList: ICard[] =
                [
                    { name: "cool sorcerycard" },
                    { name: "cool sorcery card" },
                    { name: "cool instantcard" },
                ];

            var quickSearchRoute = new quicksearch.QuickSearchRoute(cardList, 10);

            var result = quickSearchRoute.autoComplete("cery");

            assert.equal(result.length, 2);
            assert.equal(result[0].name, "cool sorcery card");
            assert.equal(result[1].name, "cool sorcerycard");
        });

        it("should sort return array by how early in the card name the query is found", () => {
            var cardList: ICard[] =
                [
                    { name: "card sorcery" },
                    { name: "instantcard" },
                    { name: "anothersorcerycard" },
                    { name: "sorcerycard" },
                ];

            var quickSearchRoute = new quicksearch.QuickSearchRoute(cardList, 10);

            var result = quickSearchRoute.autoComplete("sorcery");

            assert.equal(result.length, 3);
            assert.equal(result[0].name, "sorcerycard");
            assert.equal(result[1].name, "card sorcery");
            assert.equal(result[2].name, "anothersorcerycard");
        });

        it("should sort alphabetically if search string appears in same location", () => {
            var cardList: ICard[] =
                [
                    { name: "sorcerye" },
                    { name: "sorceryf" },
                    { name: "sorceryg" },
                    { name: "sorceryc" },
                    { name: "sorceryd" },
                    { name: "sorceryb" },
                    { name: "sorcerya" },
                ];

            var quickSearchRoute = new quicksearch.QuickSearchRoute(cardList, 10);

            var result = quickSearchRoute.autoComplete("sorcery");

            assert.equal(result.length, 7);
            assert.equal(result[0].name, "sorcerya");
            assert.equal(result[1].name, "sorceryb");
            assert.equal(result[2].name, "sorceryc");
            assert.equal(result[3].name, "sorceryd");
            assert.equal(result[4].name, "sorcerye");
            assert.equal(result[5].name, "sorceryf");
            assert.equal(result[6].name, "sorceryg");
        });

        it("should be case insensitive", () => {
            var cardList: ICard[] =
                [
                    { name: "sorcery" },
                    { name: "instant" },
                ];

            var quickSearchRoute = new quicksearch.QuickSearchRoute(cardList, 10);

            var result = quickSearchRoute.autoComplete("CERY");

            assert.equal(result.length, 1);
            assert.equal(result[0].name, "sorcery");
        });

        it("should trim query string", () => {
            var cardList: ICard[] =
                [
                    { name: "sorcery" },
                    { name: "instant" },
                ];

            var quickSearchRoute = new quicksearch.QuickSearchRoute(cardList, 10);

            var result = quickSearchRoute.autoComplete(" cery ");

            assert.equal(result.length, 1);
            assert.equal(result[0].name, "sorcery");
        });

        it("should limit return array size by maxResults", () => {
            var cardList: ICard[] =
                [
                    { name: "sorcerye" },
                    { name: "sorceryf" },
                    { name: "sorceryg" },
                    { name: "sorceryc" },
                    { name: "sorceryd" },
                    { name: "sorceryb" },
                    { name: "sorcerya" },
                    { name: "sorcerya" },
                    { name: "sorcerya" },
                    { name: "sorcerya" },
                    { name: "sorcerya" },
                    { name: "sorcerya" },
                    { name: "sorcerya" },
                    { name: "sorcerya" },
                    { name: "sorcerya" },
                    { name: "sorcerya" },
                    { name: "sorcerya" },
                ];

            var quickSearchRoute = new quicksearch.QuickSearchRoute(cardList, 5);

            var result = quickSearchRoute.autoComplete(" cery ");

            assert.equal(result.length, 5);
        });

    });
});
