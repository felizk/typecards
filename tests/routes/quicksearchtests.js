"use strict";
var quicksearch = require("../../source/routes/quicksearch");
var assert = require("assert");
describe("QuickSeachRoute", function () {
    describe("AutoComplete", function () {
        it("should return an empty array if query length is less than 3", function () {
            var cardList = [
                { name: "carda" },
                { name: "cardb" },
            ];
            var quickSearchRoute = new quicksearch.QuickSearchRoute(cardList, 10);
            var result = quickSearchRoute.autoComplete("ca");
            assert.equal(result.length, 0);
        });
        it("should return cards with names that contain the search string", function () {
            var cardList = [
                { name: "cool sorcerycard" },
                { name: "cool sorcery card" },
                { name: "cool instantcard" },
            ];
            var quickSearchRoute = new quicksearch.QuickSearchRoute(cardList, 10);
            var result = quickSearchRoute.autoComplete("cery");
            assert.equal(result.length, 2);
            assert.equal(result[0].name, "cool sorcery card");
            assert.equal(result[1].name, "cool sorcerycard");
        });
        it("should sort return array by how early in the card name the query is found", function () {
            var cardList = [
                { name: "card sorcery" },
                { name: "instantcard" },
                { name: "anothersorcerycard" },
                { name: "sorcerycard" },
            ];
            var quickSearchRoute = new quicksearch.QuickSearchRoute(cardList, 10);
            var result = quickSearchRoute.autoComplete("sorcery");
            assert.equal(result.length, 3);
            assert.equal(result[0].name, "sorcerycard");
            assert.equal(result[1].name, "card sorcery");
            assert.equal(result[2].name, "anothersorcerycard");
        });
        it("should sort alphabetically if search string appears in same location", function () {
            var cardList = [
                { name: "sorcerye" },
                { name: "sorceryf" },
                { name: "sorceryg" },
                { name: "sorceryc" },
                { name: "sorceryd" },
                { name: "sorceryb" },
                { name: "sorcerya" },
            ];
            var quickSearchRoute = new quicksearch.QuickSearchRoute(cardList, 10);
            var result = quickSearchRoute.autoComplete("sorcery");
            assert.equal(result.length, 7);
            assert.equal(result[0].name, "sorcerya");
            assert.equal(result[1].name, "sorceryb");
            assert.equal(result[2].name, "sorceryc");
            assert.equal(result[3].name, "sorceryd");
            assert.equal(result[4].name, "sorcerye");
            assert.equal(result[5].name, "sorceryf");
            assert.equal(result[6].name, "sorceryg");
        });
        it("should be case insensitive", function () {
            var cardList = [
                { name: "sorcery" },
                { name: "instant" },
            ];
            var quickSearchRoute = new quicksearch.QuickSearchRoute(cardList, 10);
            var result = quickSearchRoute.autoComplete("CERY");
            assert.equal(result.length, 1);
            assert.equal(result[0].name, "sorcery");
        });
        it("should trim query string", function () {
            var cardList = [
                { name: "sorcery" },
                { name: "instant" },
            ];
            var quickSearchRoute = new quicksearch.QuickSearchRoute(cardList, 10);
            var result = quickSearchRoute.autoComplete(" cery ");
            assert.equal(result.length, 1);
            assert.equal(result[0].name, "sorcery");
        });
        it("should limit return array size by maxResults", function () {
            var cardList = [
                { name: "sorcerye" },
                { name: "sorceryf" },
                { name: "sorceryg" },
                { name: "sorceryc" },
                { name: "sorceryd" },
                { name: "sorceryb" },
                { name: "sorcerya" },
                { name: "sorcerya" },
                { name: "sorcerya" },
                { name: "sorcerya" },
                { name: "sorcerya" },
                { name: "sorcerya" },
                { name: "sorcerya" },
                { name: "sorcerya" },
                { name: "sorcerya" },
                { name: "sorcerya" },
                { name: "sorcerya" },
            ];
            var quickSearchRoute = new quicksearch.QuickSearchRoute(cardList, 5);
            var result = quickSearchRoute.autoComplete(" cery ");
            assert.equal(result.length, 5);
        });
    });
});
//# sourceMappingURL=quicksearchtests.js.map