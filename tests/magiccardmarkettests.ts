﻿/// <reference path="../typings/mocha.d.ts" />
/// <reference path="../typings/cards.d.ts" />
import _ = require("underscore");
import mkm = require("../source/magiccardmarket");
import assert = require("assert");

describe("AuthorizationHeaderGenerator", () => {

    // Check that for a specific URL the data is correct

    describe("given specific url, nonce, time, appKey and appSecret when generating header", () => {
        var generator : mkm.AuthorizationHeaderGenerator;

        beforeEach(() => {
            generator = new mkm.AuthorizationHeaderGenerator("abcd1234abcd1234", "efgh5678efgh5678", () => "nonrandomnonce", () => 13371337);
        });

        it('generates expected signature', () => {
            var header = generator.generateAuthorizationHeader("http://localhost/");

            // This is a pregenerated signature made from a known working state:
            assert(header.indexOf('oauth_signature="/Wfuu07Q6a/KQHfpuhwzP49JxFo="') != -1);
        });

        it('should be in correct format', () => {
            var header = generator.generateAuthorizationHeader("http://localhost/");

            // Pretested regex
            assert(header.match(/OAuth (\w+="[^"]*", )+(\w+="[^"]*")/));
        });

        it('should contain appKey', () => {
            var header = generator.generateAuthorizationHeader("http://localhost/");

            assert(header.indexOf('oauth_consumer_key="abcd1234abcd1234"') != -1);
        });

        it('should contain realm', () => {
            var header = generator.generateAuthorizationHeader("http://localhost/");

            assert(header.indexOf('realm="http://localhost/"') != -1);
        });

        it('should contain nonce', () => {
            var header = generator.generateAuthorizationHeader("nonrandomnonce");

            assert(header.indexOf('oauth_nonce="nonrandomnonce"') != -1);
        });

        it('should contain signature method', () => {
            var header = generator.generateAuthorizationHeader("nonrandomnonce");

            assert(header.indexOf('oauth_signature_method="HMAC-SHA1"') != -1);
        });

        it('should contain timestamp', () => {
            var header = generator.generateAuthorizationHeader("nonrandomnonce");

            assert(header.indexOf('oauth_timestamp="13371337"') != -1);
        });

        it('should contain version', () => {
            var header = generator.generateAuthorizationHeader("nonrandomnonce");

            assert(header.indexOf('oauth_version="1.0"') != -1);
        });
    });


});


class MockCardProductService implements mkm.IMKMCardProductService {
    constructor(private cards: mkm.IMKMProduct[]) {
    }

    public request(url: string): Promise.IThenable<any> {
        return new Promise((resolve) => {
            setTimeout(() => {
                this.requestedUrl = url;
                resolve({ product: this.cards });
            }, 0);
        });
    }

    public requestedUrl: string;
}


describe("CardProductProvider", () => {
    it("should map magiccardmarket products to generic card products", done => {
        var mkmProducts : mkm.IMKMProduct[] = [
            {
                website: "/test website",
                image: "test image",
                expIcon: 1337,
                priceGuide: { SELL: 0, LOW: 0, LOWEX: 0, LOWFOIL: 0, AVG: 0, TREND: 42 }
            },
            {
                website: "/test website 2",
                image: "test image 2",
                expIcon: 1338,
                priceGuide: { SELL: 0, LOW: 0, LOWEX: 0, LOWFOIL: 0, AVG: 0, TREND: 43 }
            }];

        var serviceMock = new MockCardProductService(mkmProducts);
        var cardProductProvider = new mkm.CardProductProvider(serviceMock);

        cardProductProvider.requestCardProducts("anything").then(cards => {
            assert(cards.length == 2);

            assert.deepEqual(cards[0], {
                productPage: "https://www.magiccardmarket.eu/test website",
                image: "https://www.magiccardmarket.eu/test image",
                expIcon: 1337,
                trendingPrice: 42
            });

            assert.deepEqual(cards[1], {
                productPage: "https://www.magiccardmarket.eu/test website 2",
                image: "https://www.magiccardmarket.eu/test image 2",
                expIcon: 1338,
                trendingPrice: 43
            });

            done();
        });
    });

    it("should access the correct URL with url encoded parameter", done => {
        var mkmProducts: mkm.IMKMProduct[] = [
            {
                website: "/test website",
                image: "test image",
                expIcon: 1337,
                priceGuide: { SELL: 0, LOW: 0, LOWEX: 0, LOWFOIL: 0, AVG: 0, TREND: 42 }
            }];


        var serviceMock = new MockCardProductService(mkmProducts);
        var cardProductProvider = new mkm.CardProductProvider(serviceMock);

        cardProductProvider.requestCardProducts("card with spaces").then(cards => {
            assert(serviceMock.requestedUrl == "https://www.mkmapi.eu/ws/v1.1/output.json/products/card%20with%20spaces/1/1/false");
            done();
        });
    });
});
