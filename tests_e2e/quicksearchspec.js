﻿describe('Mobile Gatherer', function () {
    describe('QuickSearch', function () {
        
        describe('when user types in search field', function () {
            it('should show results in dropdown', function () {
                browser.get('http://typecards.azurewebsites.net/');
                
                element(by.model('lookupQuery')).sendKeys('elemental');
                
                var elements = element(by.model('lookupQuery')).element(by.xpath('..')).element(by.tagName('ul')).all(by.tagName('li'));
                expect(elements.count()).toEqual(15);
                expect(elements.get(0).getText()).toEqual("Elemental");
                expect(elements.get(14).getText()).toEqual("Blue Elemental Blast");
            });
        });
        
        describe('when dropdown item is clicked', function () {
            it('should update the text in the search field', function () {
                browser.get('http://typecards.azurewebsites.net/');
                
                element(by.model('lookupQuery')).sendKeys('elemental');
                
                var elements = element(by.model('lookupQuery')).element(by.xpath('..')).element(by.tagName('ul')).all(by.tagName('li'));
                
                elements.get(14).click();
                
                expect(element(by.model('lookupQuery')).getAttribute('value')).toEqual("Blue Elemental Blast");
            });
            
            it('should show card name, type, cost and text', function () {
                browser.get('http://typecards.azurewebsites.net/');
                
                element(by.model('lookupQuery')).sendKeys('goblin rabblemaster');
                element(by.model('lookupQuery')).sendKeys(protractor.Key.ENTER);
                
                expect(element(by.css('.card-name')).getText()).toEqual("Goblin Rabblemaster");
                expect(element(by.css('.card-type')).getText()).toContain("Goblin Warrior");
                expect(element(by.css('.card-text')).getText()).toEqual("Other Goblin creatures you control attack each turn if able. At the beginning of combat on your turn, put a 1/1 red Goblin creature token with haste onto the battlefield. Whenever Goblin Rabblemaster attacks, it gets +1/+0 until end of turn for each other attacking Goblin.");
                expect(element(by.css('.card-mana-cost')).getText()).toContain("{2}{R}");
                
                element.all(by.repeater("ruling in details.rulings")).then(function (rulings) {
                    expect(rulings[0].element(by.tagName("p")).getText()).toContain("Although Goblin Rabblemaster doesn’t force itself to attack, if you control two of them, they’ll force each other to attack if able");
                });
                
                element.all(by.repeater("legality in details.legalities")).then(function (legalities) {
                    expect(legalities[0].element(by.tagName("p")).getText()).toContain("Commander - Legal");
                });
            });

            it('should load prices and image', function () {
                browser.get('http://typecards.azurewebsites.net/');
                
                element(by.model('lookupQuery')).sendKeys('goblin rabblemaster');
                element(by.model('lookupQuery')).sendKeys(protractor.Key.ENTER);
                
                expect(element(by.css(".card-image")).getAttribute("complete")).toEqual('true');

                element.all(by.repeater("product in details.products")).then(function (products) {
                    expect(products.length).toBeGreaterThan(0);
                });
            });
        });
    });
});