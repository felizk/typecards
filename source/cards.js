"use strict";
/// <reference path="../typings/cards.d.ts" />
/// <reference path="../typings/adm-zip/adm-zip.d.ts" />
var AdmZip = require("adm-zip");
function loadCardSetFromZip(zipFile) {
    var zip = new AdmZip(zipFile);
    var entries = zip.getEntries();
    if (entries.length !== 1) {
        throw new Error("Expected a single file in card zipFile.");
    }
    return JSON.parse(entries[0].getData().toString("utf8"));
}
exports.loadCardSetFromZip = loadCardSetFromZip;
function cardSetToList(set) {
    var result = [];
    for (var key in set) {
        if (set.hasOwnProperty(key)) {
            result.push(set[key]);
        }
    }
    return result;
}
exports.cardSetToList = cardSetToList;
//# sourceMappingURL=cards.js.map