﻿/// <reference path="../typings/cards.d.ts" />
/// <reference path="../typings/cardproduct.d.ts" />
/// <reference path="../typings/promise/promise.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
import crypto = require("crypto");
import _ = require("underscore");
import Promise = require("promise");
import https = require("https");
import url = require('url');

export function makeDefaultProductProvider(appKey: string, appSecret: string): ICardProductProvider {
    return new CardProductProvider(new MKMHttpCardProductService(new AuthorizationHeaderGenerator(appKey, appSecret, () => makeNonce(8), getUnixTimestamp)));
}

export interface IMKMPriceGuide {
    SELL: number;       // Average price of articles ever sold of this product
    LOW: number;        // Current lowest non-foil price (all conditions)
    LOWEX: number;      // Current lowest non-foil price (condition EX and better)
    LOWFOIL: number;    // Current lowest foil price
    AVG: number;        // Current average non-foil price of all available articles of this product
    TREND: number;      // Current trend price
}

export interface IMKMProduct {
    website: string;            // URL to the product (relative to MKM's base URL)
    image: string;              // Path to the product's image
    expIcon: number;            // Index of the expansion icon
    priceGuide: IMKMPriceGuide; // Price guide entity '''(ATTN: not returned for expansion requests)'''
}

interface MKMHeaderParam {
    key: string,
    value: string
}

export interface OAuthAuthorizationHeaderGenerator {
    generateAuthorizationHeader(url: string);
}

export interface ICardProductProvider {
    requestCardProducts(cardName: string): Promise.IThenable<ICardProductDetails[]>;
}

export class CardProductProvider implements ICardProductProvider {
    constructor(private priceService: IMKMCardProductService) {
    }

    public requestCardProducts(cardName: string): Promise.IThenable<ICardProductDetails[]> {
        return new Promise((resolve, reject) => {
            var url = "https://www.mkmapi.eu/ws/v1.1/output.json/products/" + encodeURIComponent(cardName) + "/1/1/false";

            this.priceService.request(url).then((dataObj) => {
                if (dataObj && dataObj.product && dataObj.product.length > 0) {
                    var mkmProducts : IMKMProduct[] = dataObj.product;
                    resolve(mkmProducts.map(CardProductProvider.mkmProductToCardProduct));
                } else {
                    reject("no products found");
                }
            }, reject);
        });
    }

    public static mkmProductToCardProduct(mkmProduct: IMKMProduct): ICardProductDetails {
        return {
            productPage: "https://www.magiccardmarket.eu" + mkmProduct.website,
            image: "https://www.magiccardmarket.eu/" + mkmProduct.image,
            expIcon: mkmProduct.expIcon,
            trendingPrice: mkmProduct.priceGuide.TREND || 0
        };
    }
}

export class AuthorizationHeaderGenerator {
    constructor(private appKey: string, private appSecret: string, private nonceFunc: () => string, private currentTimeInSecondsFunc: () => number) {
    }

    public generateAuthorizationHeader(url: string): string {
        var params: MKMHeaderParam[] = [];

        // These need to be alphabetised for the OAuth signature.
        params.push({ key: "oauth_consumer_key", value: this.appKey });
        params.push({ key: "oauth_nonce", value: this.nonceFunc() });
        params.push({ key: "oauth_signature_method", value: "HMAC-SHA1" });
        params.push({ key: "oauth_timestamp", value: this.currentTimeInSecondsFunc().toString() });
        params.push({ key: "oauth_token", value: this.accessToken });
        params.push({ key: "oauth_version", value: "1.0" });

        // First we generate the signature for the Authorization header.
        // We need to build a string that contains all the relevant data for our request.
        // Then that string is hashed using our app secret.
        // The server we are connecting to will recreate the same string from the data we send it in our header and its copy of the secret.
        // If they don't match, our request has been tampered with and will be rejected.
        // That's why the order matters in the above array initialization.
        var signatureParameters = _.map(params, param => encodeURIComponent(param.key) + "=" + encodeURIComponent(param.value))
            .join("&");

        var signatureData = "GET&" + encodeURIComponent(url) + "&" + encodeURIComponent(signatureParameters);
        var signatureKey = encodeURIComponent(this.appSecret) + "&" + this.accessSecret;
        var oauthSignature = crypto.createHmac('sha1', signatureKey).update(signatureData).digest('base64');

        // Now we need to populate the Authorization header with our signature and all the data we used to create it.
        // Except our secret of course.
        params.push({ key: "oauth_signature", value: oauthSignature });

        // Usually this isn't required, but the MKM api specifically requires it in their documentation.
        params.push({ key: "realm", value: url });

        var headerParameters = _.map(params, param => param.key + '="' + param.value + '"')
            .join(", ");

        return "OAuth " + headerParameters;
    }

    // We may want to support these in the future if we ever want to access user specific information
    private accessToken = "";
    private accessSecret = "";
}

export interface IMKMCardProductService {
    request(url: string): Promise.IThenable<any>;
}

export class MKMHttpCardProductService implements IMKMCardProductService {
    constructor(private headerGenerator: OAuthAuthorizationHeaderGenerator) {
    }

    public request(path: string): Promise.IThenable<any> {
        return new Promise((resolve, reject) => {
            var urlParts = url.parse(path);

            var requestOptions = {
                hostname: urlParts.hostname,
                protocol: urlParts.protocol,
                path: urlParts.path,
                method: "GET",
                headers: {
                    Authorization: this.headerGenerator.generateAuthorizationHeader(urlParts.href)
                },
                rejectUnauthorized: false
            };

            var request =
                https.request(requestOptions, response => {
                    var body = '';

                    response.on('data', data => {
                        body += data;
                    });

                    response.on('end', () => {
                        if (response.statusCode == 200) {
                            resolve(JSON.parse(body));
                        } else {
                            reject(response.statusCode + "");
                        }
                    });
                });

            request.on("error", (error) => {
                reject(error);
            });

            request.end();
        });
    }
}

export function makeNonce(length: number) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

export function getUnixTimestamp(): number {
    return Math.floor((new Date).getTime() / 1000);
}