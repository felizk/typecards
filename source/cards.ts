﻿/// <reference path="../typings/cards.d.ts" />
/// <reference path="../typings/adm-zip/adm-zip.d.ts" />
import AdmZip = require("adm-zip");

export function loadCardSetFromZip(zipFile: string): ICardSet {
    var zip = new AdmZip(zipFile);
    var entries = zip.getEntries();

    if (entries.length !== 1) {
        throw new Error("Expected a single file in card zipFile.");
    }

    return JSON.parse(entries[0].getData().toString("utf8"));
}

export function cardSetToList(set: ICardSet): ICard[] {
    var result: ICard[] = [];

    for (var key in set) {
        if (set.hasOwnProperty(key)) {
            result.push(set[key]);
        }
    }

    return result;
}