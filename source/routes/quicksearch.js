"use strict";
var _ = require("underscore");
var QuickSearchRoute = (function () {
    function QuickSearchRoute(cardList, maxResults) {
        var _this = this;
        if (maxResults === void 0) { maxResults = 15; }
        this.maxResults = maxResults;
        this.route = function (req, res) {
            res.json(_this.autoComplete(req.query.q));
        };
        // Create an uppecase name lookup for case insensitive search.
        this.upperCaseLookup = cardList.map(function (card) {
            return { name: card.name.toLocaleUpperCase(), card: card };
        });
    }
    QuickSearchRoute.prototype.autoComplete = function (query) {
        var _this = this;
        if (!query || !query.length)
            return [];
        query = query.trim().toLocaleUpperCase();
        if (query.length < 3)
            return [];
        var results = _.filter(this.upperCaseLookup, function (entry) { return entry.name.indexOf(query) > -1; });
        results.sort(function (a, b) { return _this.cardComparer(a.name, b.name, query); });
        return _.first(results, this.maxResults).map(function (entry) { return entry.card; });
    };
    QuickSearchRoute.prototype.cardComparer = function (a, b, query) {
        var idxSort = a.indexOf(query) - b.indexOf(query);
        if (idxSort != 0) {
            return idxSort;
        }
        return a.localeCompare(b, "en-US");
    };
    return QuickSearchRoute;
}());
exports.QuickSearchRoute = QuickSearchRoute;
//# sourceMappingURL=quicksearch.js.map