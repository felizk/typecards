﻿/// <reference path="../../typings/express/express.d.ts" />
/// <reference path="../../typings/underscore/underscore.d.ts" />
/// <reference path="../../typings/cards.d.ts" />
import express = require("express");
import _ = require("underscore");
import cards = require("../cards");

export class QuickSearchRoute {

    constructor(cardList: ICard[], private maxResults = 15) {
        // Create an uppecase name lookup for case insensitive search.
        this.upperCaseLookup = cardList.map(card =>
        {
            return { name: card.name.toLocaleUpperCase(), card: card };
        });
    }

    route = (req: express.Request, res: express.Response) => {
        res.json(this.autoComplete(req.query.q));
    };

    public autoComplete(query: string): ICard[] {
        if (!query || !query.length)
            return [];

        query = query.trim().toLocaleUpperCase();

        if (query.length < 3)
            return [];

        var results = _.filter(this.upperCaseLookup, entry => entry.name.indexOf(query) > -1);
        results.sort((a,b) => this.cardComparer(a.name, b.name, query));

        return _.first(results, this.maxResults).map(entry => entry.card);
    }

    private cardComparer(a: string, b: string, query: string): number {
        var idxSort = a.indexOf(query) - b.indexOf(query);
        if (idxSort != 0) {
            return idxSort;
        }

        return a.localeCompare(b, "en-US");
    }

    private upperCaseLookup: { name: string, card: ICard }[];
}