"use strict";
var ProductSearchRoute = (function () {
    function ProductSearchRoute(productProvider) {
        var _this = this;
        this.productProvider = productProvider;
        this.route = function (req, res) {
            _this.productProvider.requestCardProducts(req.query.q).then(function (products) {
                res.json(products);
            }, function (error) {
                res.status(404);
                res.end();
            });
        };
    }
    return ProductSearchRoute;
}());
exports.ProductSearchRoute = ProductSearchRoute;
//# sourceMappingURL=productsearch.js.map