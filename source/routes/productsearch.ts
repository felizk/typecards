﻿/// <reference path="../../typings/express/express.d.ts" />
import express = require("express");
import mkm = require("../magiccardmarket");

export class ProductSearchRoute {

    constructor(private productProvider: mkm.ICardProductProvider) {}

    route = (req: express.Request, res: express.Response) => {
        this.productProvider.requestCardProducts(req.query.q).then(products => {
            res.json(products);
        }, error => {
            res.status(404);
            res.end();
        });
    };
}