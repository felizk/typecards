"use strict";
/// <reference path="../typings/cards.d.ts" />
/// <reference path="../typings/cardproduct.d.ts" />
/// <reference path="../typings/promise/promise.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
var crypto = require("crypto");
var _ = require("underscore");
var Promise = require("promise");
var https = require("https");
var url = require('url');
function makeDefaultProductProvider(appKey, appSecret) {
    return new CardProductProvider(new MKMHttpCardProductService(new AuthorizationHeaderGenerator(appKey, appSecret, function () { return makeNonce(8); }, getUnixTimestamp)));
}
exports.makeDefaultProductProvider = makeDefaultProductProvider;
var CardProductProvider = (function () {
    function CardProductProvider(priceService) {
        this.priceService = priceService;
    }
    CardProductProvider.prototype.requestCardProducts = function (cardName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = "https://www.mkmapi.eu/ws/v1.1/output.json/products/" + encodeURIComponent(cardName) + "/1/1/false";
            _this.priceService.request(url).then(function (dataObj) {
                if (dataObj && dataObj.product && dataObj.product.length > 0) {
                    var mkmProducts = dataObj.product;
                    resolve(mkmProducts.map(CardProductProvider.mkmProductToCardProduct));
                }
                else {
                    reject("no products found");
                }
            }, reject);
        });
    };
    CardProductProvider.mkmProductToCardProduct = function (mkmProduct) {
        return {
            productPage: "https://www.magiccardmarket.eu" + mkmProduct.website,
            image: "https://www.magiccardmarket.eu/" + mkmProduct.image,
            expIcon: mkmProduct.expIcon,
            trendingPrice: mkmProduct.priceGuide.TREND || 0
        };
    };
    return CardProductProvider;
}());
exports.CardProductProvider = CardProductProvider;
var AuthorizationHeaderGenerator = (function () {
    function AuthorizationHeaderGenerator(appKey, appSecret, nonceFunc, currentTimeInSecondsFunc) {
        this.appKey = appKey;
        this.appSecret = appSecret;
        this.nonceFunc = nonceFunc;
        this.currentTimeInSecondsFunc = currentTimeInSecondsFunc;
        // We may want to support these in the future if we ever want to access user specific information
        this.accessToken = "";
        this.accessSecret = "";
    }
    AuthorizationHeaderGenerator.prototype.generateAuthorizationHeader = function (url) {
        var params = [];
        // These need to be alphabetised for the OAuth signature.
        params.push({ key: "oauth_consumer_key", value: this.appKey });
        params.push({ key: "oauth_nonce", value: this.nonceFunc() });
        params.push({ key: "oauth_signature_method", value: "HMAC-SHA1" });
        params.push({ key: "oauth_timestamp", value: this.currentTimeInSecondsFunc().toString() });
        params.push({ key: "oauth_token", value: this.accessToken });
        params.push({ key: "oauth_version", value: "1.0" });
        // First we generate the signature for the Authorization header.
        // We need to build a string that contains all the relevant data for our request.
        // Then that string is hashed using our app secret.
        // The server we are connecting to will recreate the same string from the data we send it in our header and its copy of the secret.
        // If they don't match, our request has been tampered with and will be rejected.
        // That's why the order matters in the above array initialization.
        var signatureParameters = _.map(params, function (param) { return encodeURIComponent(param.key) + "=" + encodeURIComponent(param.value); })
            .join("&");
        var signatureData = "GET&" + encodeURIComponent(url) + "&" + encodeURIComponent(signatureParameters);
        var signatureKey = encodeURIComponent(this.appSecret) + "&" + this.accessSecret;
        var oauthSignature = crypto.createHmac('sha1', signatureKey).update(signatureData).digest('base64');
        // Now we need to populate the Authorization header with our signature and all the data we used to create it.
        // Except our secret of course.
        params.push({ key: "oauth_signature", value: oauthSignature });
        // Usually this isn't required, but the MKM api specifically requires it in their documentation.
        params.push({ key: "realm", value: url });
        var headerParameters = _.map(params, function (param) { return param.key + '="' + param.value + '"'; })
            .join(", ");
        return "OAuth " + headerParameters;
    };
    return AuthorizationHeaderGenerator;
}());
exports.AuthorizationHeaderGenerator = AuthorizationHeaderGenerator;
var MKMHttpCardProductService = (function () {
    function MKMHttpCardProductService(headerGenerator) {
        this.headerGenerator = headerGenerator;
    }
    MKMHttpCardProductService.prototype.request = function (path) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var urlParts = url.parse(path);
            var requestOptions = {
                hostname: urlParts.hostname,
                protocol: urlParts.protocol,
                path: urlParts.path,
                method: "GET",
                headers: {
                    Authorization: _this.headerGenerator.generateAuthorizationHeader(urlParts.href)
                },
                rejectUnauthorized: false
            };
            var request = https.request(requestOptions, function (response) {
                var body = '';
                response.on('data', function (data) {
                    body += data;
                });
                response.on('end', function () {
                    if (response.statusCode == 200) {
                        resolve(JSON.parse(body));
                    }
                    else {
                        reject(response.statusCode + "");
                    }
                });
            });
            request.on("error", function (error) {
                reject(error);
            });
            request.end();
        });
    };
    return MKMHttpCardProductService;
}());
exports.MKMHttpCardProductService = MKMHttpCardProductService;
function makeNonce(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}
exports.makeNonce = makeNonce;
function getUnixTimestamp() {
    return Math.floor((new Date).getTime() / 1000);
}
exports.getUnixTimestamp = getUnixTimestamp;
//# sourceMappingURL=magiccardmarket.js.map