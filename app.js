"use strict";
var express = require("express");
var http = require("http");
var path = require("path");
var cards = require("./source/cards");
var mkm = require("./source/magiccardmarket");
var quicksearch = require("./source/routes/quicksearch");
var productsearch = require("./source/routes/productsearch");
// Setup app
var app = express();
app.set("port", process.env.PORT || 3000);
app.use(express.favicon());
app.use(express.logger("dev"));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, "public")));
if ("development" === app.get("env")) {
    app.use(express.errorHandler());
    // Necessary to lookup the ts files for the js files.
    app.use("/TypeCards.Frontend", express.static(path.join(__dirname, "TypeCards.Frontend")));
}
// Load all cards from zip into memory.
var cardSet = cards.loadCardSetFromZip("./data/AllCards-x.json.zip");
var cardList = cards.cardSetToList(cardSet);
// Setup QuickSearch
var quickSearchRoute = new quicksearch.QuickSearchRoute(cardList);
app.get("/quicksearch", quickSearchRoute.route);
// Setup ProductSearch (for prices)
var productSearchRoute = new productsearch.ProductSearchRoute(mkm.makeDefaultProductProvider(process.env["MKMKEY"], process.env["MKMSECRET"]));
app.get("/productsearch", productSearchRoute.route);
// Start Web-Server
http.createServer(app).listen(app.get("port"), function () {
    console.log("Express server listening on port " + app.get("port"));
});
//# sourceMappingURL=app.js.map